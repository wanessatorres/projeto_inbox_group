<?php

use Illuminate\Database\Seeder;
use App\Products;

class ProductTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = new Products;
        $user->name = "Nintendo Switch";
        $user->value = 299.90;
        $user->description = 'What you need to know – This product has a serial number that uniquely identifies the item. When your order ships, Amazon will scan the serial number and add it to the history of this order. Should the item go missing before it arrives, Amazon may register the serial number with loss and theft databases, preventing fraudulent use or reselling of the item. There is no action required from you and the serial number will only be used to prevent fraudulent activity of the missing item.';
        $user->image = 'https://images-na.ssl-images-amazon.com/images/I/81lKw6WyRtL._AC_.jpg';
        $user->save();

        $user = new Products;
        $user->name = "Super Mario Odyssey - Nintendo Switch";
        $user->value = 69.90;
        $user->description = 'Explore huge 3D kingdoms filled with secrets and surprises';
        $user->image = 'https://images-na.ssl-images-amazon.com/images/I/91SF0Tzmv4L._AC_SL1500_.jpg';
        $user->save();

        $user = new Products;
        $user->name = "The Legend of Zelda: Breath of the Wild - Nintendo Switch";
        $user->value = 69.90;
        $user->description = 'Explore the wilds of Hyrule any way you like—anytime, anywhere! - ';
        $user->image = 'https://images-na.ssl-images-amazon.com/images/I/71y3rzfuUlL._AC_SL1000_.jpg';
        $user->save();
        
        $user = new Products;
        $user->name = "HORI Compact Playstand for Nintendo Switch Officially Licensed by Nintendo";
        $user->value = 250.00;
        $user->description = 'Officially Licensed by Nintendo';
        $user->image = 'https://images-na.ssl-images-amazon.com/images/I/61Ly9jIq-cL._AC_.jpg';
        $user->save();

        $user = new Products;
        $user->name = "amFilm Tempered Glass Screen Protector for Nintendo Switch 2017 (2-Pack)";
        $user->value = 14.00;
        $user->description = 'Specifically designed for Nintendo Switc';
        $user->image = 'https://images-na.ssl-images-amazon.com/images/I/71vb0EZF7UL._SL1500_.jpg';
        $user->save();
  
      }
  }
