@extends('layout.master')

@section('title', 'Shop')

@section('content')

  <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- .title -->
                        <div class="title pull-left">
                            <h1>Login</h1>
                        </div> <!-- /.title -->
                        <!-- .page-breadcumb -->
                        <div class="page-breadcumb pull-right">
                            <i class="fa fa-home"></i> <a href="/">Home</a> <i class="fa fa-angle-right"></i> <span>Login</span>
                        </div> <!-- /.page-breadcumb -->
                    </div>
                </div>
            </div>
        </section> <!-- /#page-title -->  
    <!-- #contact-content -->
    <section id="contact-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <form action="/login" method = "post" class="contact-form">
                    {!! csrf_field() !!}
                    <p>Enter with your email and password</p>
                        <p>Email</p>
                        <p><input type="hidden" name="idProduto" placeholder="produto" value = "{{$idProduto}}"></p>
                        <p><input type="email" name="email" placeholder="Email"></p>
                        <p>Senha</p>
                        <p><input type="password" name="password" placeholder="Password"></p>
                        <p><button type="submit">Go</button></p>
                    </form>
                </div>
            </div>
        </div>
    </section> <!-- /#contact-content -->




    
@endsection