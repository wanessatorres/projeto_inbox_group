<!DOCTYPE html>
<html lang="en">
    <head>
  
        <meta charset="UTF-8">
        <title>Wan´s Store</title>

        <!-- Responsive Meta Tag -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- main stylesheet -->
        <link rel="stylesheet" href="/css/style.css">
        <!-- responsive stylesheet -->
        <link rel="stylesheet" href="/css/responsive.css">


        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="js/respond.js"></script>
        <![endif]-->


    </head>
    <body>

        <!-- preloader -->
        <div class="preloader"></div>

        <!-- header -->
        <header>
            
           
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-lg-offset-0 col-md-offset-4 logo">
                        <a href="/">
                            <img src="/img/resources/logo.png" alt="Plumberx">
                        </a>
                        
                        <select class = "form-control fator_calculo">
                                <option value = "EUR" <?php if($moeda == 'EUR'){ echo 'selected'; }?>>EUR</option>
                                <option value = "BRL" <?php if($moeda == 'BRL'){ echo 'selected'; }?>>BRL</option>
                                <option value = "USD" <?php if($moeda == 'USD'){ echo 'selected'; }?>>USD</option>
                            </select>
                    </div>

                    <nav class="col-lg-9 col-md-12 col-lg-pull-0 col-md-pull-1 mainmenu-container">
                        <ul class="top-icons-wrap pull-right">
                            <li class="top-icons"><a href="/cart"><i class="icon icon-ShoppingCart"></i></a></li>
                        </ul>
                        <button class="mainmenu-toggler">
                            <i class="fa fa-bars"></i>
                        </button>

                        <ul class="mainmenu pull-right">
                            
                            <li class="dropdown">
                                <a href="/" class="hvr-overline-from-left">Home</a>
                            </li>
                            
                        </ul>
                    </nav>
                </div>
            </div>
        </header> <!-- /header -->

         @yield('content')

        <!-- footer -->
    

        <!-- #bottom-bar -->
        <section id="bottom-bar">
            <div class="container">
                <div class="row">
                    <!-- .copyright -->
                    <!-- .credit -->
                    <div class="credit pull-right">
                        <p>Created by: Wanessa Torres</p>
                    </div> <!-- /.credit -->
                </div>
            </div> 
        </section><!-- /#bottom-bar -->



        <script src="/js/jquery.min.js"></script> <!-- jQuery JS -->
        <script src="/js/bootstrap.min.js"></script> <!-- BootStrap JS -->
        <script src="http://maps.google.com/maps/api/js"></script> <!-- Gmap Helper -->
        <script src="/js/gmap.js"></script> <!-- gmap JS -->
        <script src="/js/wow.js"></script> <!-- WOW JS -->
        <script src="/js/isotope.pkgd.min.js"></script> <!-- iSotope JS -->
        <script src="/js/owl.carousel.min.js"></script> <!-- OWL Carousle JS -->
        <script src="/js/jquery.fancybox.pack.js"></script> <!-- FancyBox -->
        <script src="/js/jquery.easing.min.js"></script> <!-- EaseIng JS -->
        <script src="/js/custom.js"></script> <!-- Custom JS -->

    </body>
</html>