@extends('layout.master')

@section('title', 'Shop')

@section('content')

<section id="page-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- .title -->
                <div class="title pull-left">
                    <h1>Success</h1>
                </div> <!-- /.title -->
                <!-- .page-breadcumb -->
                <div class="page-breadcumb pull-right">
                    <i class="fa fa-home"></i> <a href="/">Home</a> <i class="fa fa-angle-right"></i> <span>Success</span>
                </div> <!-- /.page-breadcumb -->
            </div>
        </div>
    </div>
</section> <!-- /#page-title -->  
<!-- #contact-content -->
<section id="contact-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <p>Your request was successful.</p>
                <p>You will receive an email within a few minutes</p>
                <p>Thank you</p>
            </div>
        </div>
    </div>
</section> <!-- /#contact-content -->





@endsection