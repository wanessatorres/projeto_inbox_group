@extends('layout.master')

@section('title', 'Shop')

@section('content')
<section id="page-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- .title -->
                <div class="title pull-left">
                    <h1>Cart</h1>
                </div> <!-- /.title -->
                <!-- .page-breadcumb -->
                <div class="page-breadcumb pull-right">
                    <i class="fa fa-home"></i> <a href="/">Home</a> <i class="fa fa-angle-right"></i> <span>Cart</span>
                </div> <!-- /.page-breadcumb -->
            </div>
        </div>
    </div>
</section> <!-- /#page-title -->  
<section class="cart-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-3">
                <table class="table cart-table">
                    <thead>
                        <tr>
                            <th class="preview">Preview</th>
                            <th class="product">Product</th>
                            <th class="product">Price</th>
                            <th class="quantity">Quantity</th>
                            <th class="total">Total</th>
                            <th class="del-item">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $valorFinal = 0; ?>
                        @foreach($produtos as $prod)
                        <?php
                        
                        if (isset($moeda)) {
                            if ($moeda == 'EUR') {
                                $moedasim = '€';
                                $fator = 1;
                            }

                            if ($moeda == "USD") {
                                $moedasim = "$";
                            }

                            if ($moeda == "BRL") {
                                $moedasim = "R$";
                            }
                        } else {

                            $moedasim = '€';
                            $fator = 1;
                        }
                        ?>
                        <tr>
                            <td class="preview">
                                <img src="{{$prod->image}}" alt="" style="width:20%">
                            </td>
                            <td class="product">
                                {{$prod->name}}
                            </td>

                            <td>
                                <div class ='preco_euro'>{{$prod->value}}</div>
                                <div class ='price'><span class = "moeda">@if(isset($moedasim)) {{$moedasim}} @else € @endif </span> <span class = "valor_preco">{{round($prod->value*$fator,2)}}</span></div>
                            </td>
                            <td class="quantity">
                                {{$prod->quantidade}}
                            </td>
                            <td class="total">
                                <div class ='quantidade'>{{$prod->quantidade}}</div>
                                <div class ='preco_euro'>{{$prod->value}}</div>
                                <div class ='price_total'>@if(isset($moedasim)) {{$moedasim}} @else € @endif {{round(($prod->value*$prod->quantidade)*$fator,2)}}</div>
                                <?php $valorFinal = $valorFinal + (($prod->value * $prod->quantidade) * $fator); ?>
                            </td>
                            <td class="del-item">
                                <a href = "/del_item/{{$prod->id}}/{{$prod->pedido_id}}"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="cart-total-box row">
            <div class="col-lg-5 col-sm-6 col-xs-12 pull-right">
                <ul>
                    <li class="cart-total">Cart Total</li>
                    <li class="total">Total <span class = "final_value">@if(isset($moedasim)) {{$moedasim}} @else € @endif {{round($valorFinal,2)}}</span></li>
                    <li class="proceed-to-checkout"><a href="/checkout/{{$pedido->id}}" class="hvr-bounce-to-right">Proceed to checkout</a></li>
                </ul>
            </div>
        </div>
    </div>  
</section>





@endsection