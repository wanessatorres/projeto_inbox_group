@extends('layout.master')

@section('title', 'Compras')

@section('content')
    <!-- #page-title -->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- .title -->
                        <div class="title pull-left">
                            <h1>Shop</h1>
                        </div> <!-- /.title -->
                        <!-- .page-breadcumb -->
                        <div class="page-breadcumb pull-right">
                            <i class="fa fa-home"></i> <a href="/">Home</a> <i class="fa fa-angle-right"></i> <span>Shop</span>
                        </div> <!-- /.page-breadcumb -->
                    </div>
                </div>
            </div>
        </section> <!-- /#page-title -->

        <!-- #blog-post -->
        <section id="blog-post">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 shop-page-content">
                        <div class="section-title-style-2">
                            <h1>Products</h1>
                        </div>	
                       
                        <div class="row">
                            @foreach($products as $prod)           
                                <div class="col-lg-4 single-shop-item">
                                    <img src="{{$prod->image}}" alt="" class="image_product">
                                    <div class="meta">
                                        <h4>{{substr($prod->name, 0,40)}}</h4>
                                        <p>{{substr($prod->description, 0,50)}}</p>
                                        <span>Price: <span class="moeda">
                                        <?php 
                                      
                                        if(isset($moeda)){
                                            if($moeda == 'EUR'){
                                                $moedasim = '€';
                                                $fator = 1;
                                            }
                                            
                                            if($moeda == "USD"){
                                               $moedasim = "$";
                                               
                                            }
                                            
                                            if($moeda == "BRL"){
                                               $moedasim = "R$";
                                            }
                                            
                                        }else{
                                           $moedasim = '€';
                                                $fator = 1; 
                                        }
                                        ?>
                                        {{$moedasim}}</span> <b class='valor_real'>{{$prod->value}}</b> <b class='valor_preco'> {{round($prod->value*$fator,2)}}</b></span>
                                        <a href="/addProduto/{{$prod->id}}" class="add-to-cart hvr-bounce-to-right">Add to cart</a>
                                    </div>
                                </div>
                            @endforeach    
                        </div>		
                        

                    </div> <!-- /.shop-page-content -->


                </div>
            </div>
        </section> <!-- /#blog-post -->
@endsection