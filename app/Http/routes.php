<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'IndexController@index');
Route::GET('/getFatorCalculo/{fator}', 'IndexController@getFator');  
Route::GET('/addProduto/{idProduto}', 'IndexController@addProduto');
Route::POST('/login', 'LoginController@login');
Route::GET('/cart/{idCarrinho}', 'IndexController@cart');
Route::GET('/cart',"IndexController@validaCarrinho");
Route::GET('/del_item/{itemCarrinho}/{idPedido}', 'IndexController@delete_item');
Route::GET('/checkout/{idPedido}',"IndexController@checkout");
  
