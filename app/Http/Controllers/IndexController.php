<?php

namespace App\Http\Controllers;

use App\Products;
use App\Http\Controllers\Controller;
use App\Pedido;
use App\PedidoProduto;
use Cookie;
use Auth;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class IndexController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index() {
        $products = Products::all();
        $moeda = Cookie::get('moeda');
        $fator = Cookie::get('fator');
        return view('index', ['products' => $products, 'moeda' => $moeda, 'fator' => $fator]);
    }

    public function getFator($pais) {
        $XMLContent = file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        foreach ($XMLContent as $line) {
            if (preg_match("/currency='([[:alpha:]]+)'/", $line, $currencyCode)) {
                if (preg_match("/rate='([[:graph:]]+)'/", $line, $rate)) {
                    //Output the value of 1EUR for a currency code
                    if ($currencyCode[1] == $pais) {
                        $moeda = $currencyCode[1];
                        $fator = $rate[1];
                    }
                }
            }
        }
        if ($pais == 'EUR') {
            $moeda = "EUR";
            $fator = 1;
        }
        Cookie::queue('fator', $fator, 60);
        Cookie::queue('moeda', $moeda, 60);
        return array("moeda" => $moeda, "fator" => $fator);
    }

    public function addProduto($idProduto) {
        if (Auth::check()) {
            $usuario = Auth::user()->id;

            $pedidoExistente = Pedido::where('user_id', $usuario)->where('status', 0)->count();
            if ($pedidoExistente == 0) {
                $pedido = new Pedido();
                $pedido->user_id = $usuario;
                $pedido->status = 0;
                $pedido->save();

                $produtoExistente = PedidoProduto::where('pedido_id', $pedido->id)->where('product_id', $idProduto)->count();
                if ($produtoExistente == 0) {
                    $pedidoproduto = new PedidoProduto();
                    $pedidoproduto->pedido_id = $pedido->id;
                    $pedidoproduto->product_id = $idProduto;
                    $pedidoproduto->quantidade = 1;
                    $pedidoproduto->save();
                } else {
                    $pedidoprodutos = PedidoProduto::where('pedido_id', $pedido->id)->where('product_id', $idProduto)->first();
                    $pedidoprodutos->quantidade = $pedidoprodutos->quantidade + 1;
                    $pedidoprodutos->save();
                }
            } else {
                $pedido = Pedido::where('user_id', $usuario)->where('status', 0)->first();
                $produtoExistente = PedidoProduto::where('pedido_id', $pedido->id)->where('product_id', $idProduto)->count();
                if ($produtoExistente == 0) {
                    $pedidoproduto = new PedidoProduto();
                    $pedidoproduto->pedido_id = $pedido->id;
                    $pedidoproduto->product_id = $idProduto;
                    $pedidoproduto->quantidade = 1;
                    $pedidoproduto->save();
                } else {
                    $pedidoprodutos = PedidoProduto::where('pedido_id', $pedido->id)->where('product_id', $idProduto)->first();
                    $pedidoprodutos->quantidade = $pedidoprodutos->quantidade + 1;
                    $pedidoprodutos->save();
                }
            }
            return redirect('cart/' . $pedido->id);
        } else {
            $moeda = Cookie::get('moeda');
            $fator = Cookie::get('fator');
            return view('login', ['idProduto' => $idProduto, 'moeda' => $moeda, 'fator' => $fator]);
        }
    }

    public function cart($idCarrinho) {
        $moeda = Cookie::get('moeda');
        $fator = Cookie::get('fator');
        $pedido = Pedido::where('id', $idCarrinho)->first();
        $produtos = PedidoProduto::getProdutos($idCarrinho);
        $conta = PedidoProduto::getProdutosConta($idCarrinho);
        $valorFinal = PedidoProduto::getValorFinal($idCarrinho) * $fator;

        return view('cart', ['moeda' => $moeda, 'fator' => $fator, "pedido" => $pedido, "produtos" => $produtos, 'conta' => $conta, 'valor_final' => $valorFinal]);
    }

    public function delete_item($idItem, $idPedido) {

        $produtos = PedidoProduto::getProdutos($idPedido);

        $produto = PedidoProduto::find($idItem);
        $produto->delete();



        return redirect('cart/' . $idPedido);
    }

    public function validaCarrinho() {
        if (Auth::check()) {
            $usuario = Auth::user()->id;
            $pedidoExistente = Pedido::where('user_id', $usuario)->where('status', 0)->count();
            if ($pedidoExistente == 0) {
                $pedido = new Pedido();
                $pedido->user_id = $usuario;
                $pedido->status = 0;
                $pedido->save();
            } else {
                $pedido = Pedido::where('user_id', $usuario)->where('status', 0)->first();
            }
            return redirect('cart/' . $pedido->id);
        } else {
            $moeda = Cookie::get('moeda');
            $fator = Cookie::get('fator');
            return view('login', ['idProduto' => $idProduto, 'moeda' => $moeda, 'fator' => $fator]);
        }
    }

    public function checkout($idPedido) {
        $moeda = Cookie::get('moeda');
        $fator = Cookie::get('fator');
        $pedido = Pedido::find($idPedido);
        $pedido->status = 1;
        $pedido->save();


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->SMTPSecure = "tls";
            $mail->Host = "smtp.gmail.com";
            $mail->Port = "587"; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'kats.the@gmail.com';                 // SMTP username
            $mail->Password = 'wantorres28';                           // SMTP password
                                           // TCP port to connect to
            //Recipients
            $mail->setFrom('admin@lojadawan.com', 'Loja da Wan');
            $mail->addAddress('kats.the@gmail.com', 'Joe User');     // Add a recipient
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Here is the confirmation from loja da wan';
            $mail->Body = 'Your request was successful ! Congratulations</b>';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
        return view('confirma', ['moeda' => $moeda, 'fator' => $fator]);
    }

}
