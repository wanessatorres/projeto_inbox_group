<?php

namespace App\Http\Controllers;

use App\Products;
use App\Http\Controllers\Controller;
use App\Pedido;
use Cookie;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Hash;

class LoginController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
   
    public function login(Request $request)
    {
        $usuario = User::where('email','=',$request->get('email'))->first();

        if ($usuario && Hash::check($request->get('password'), $usuario->password)){
            Auth::login($usuario);
            
            return redirect('cart'); 
        } else {
           return redirect('login'); 
        }
    }
    public function logout()
    {
        
    }
    
    
}