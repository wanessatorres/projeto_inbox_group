<?php

namespace App;
use App\Products;
use Illuminate\Database\Eloquent\Model;

class PedidoProduto extends Model
{
    protected $table = 'pedido_produto';
    
    public static function getProdutos($pedido_id){
    	return $prods = PedidoProduto::where('pedido_id',$pedido_id)
        ->select('pedido_produto.*', 'product.name', 'product.value', "product.image")
    	->join('product','product.id','=','pedido_produto.product_id')
    	->get();


    }
    public static function getProdutosConta($pedido_id){
    	return $prods = PedidoProduto::where('pedido_id',$pedido_id)
        ->select('pedido_produto.*', 'product.name', 'product.value', "product.image")
    	->join('product','product.id','=','pedido_produto.product_id')
    	->count();


    }
    public static function getValorFinal($pedido_id){
        $prods = PedidoProduto::where('pedido_id',$pedido_id)
        ->select('pedido_produto.*', 'product.name', 'product.value', "product.image")
    	->join('product','product.id','=','pedido_produto.product_id')
    	->get();
        $valorFinal = 0;
        foreach($prods as $prod){
            $valorFinal = $valorFinal + $prod->quantidade*$prod->value;
        }
        return($valorFinal);
    }
   
}
